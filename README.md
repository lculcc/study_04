### 作业一：

第1种方式：Method01 采用循环等待方式

第2种方式：Method02 采用join

第3种方式：Method03 采用同步块 + wait/notifyAll方式

第4种方式：Method04 采用lock + condition

第5种方式：Method05 采用Semaphore（未能实现，需要让主线程睡眠才可获取结果）

第6种方式：Method06 采用CyclicBarrier

第7种方式：Method07 采用CountDownLatch

第8种方式：Method08 采用Callable+future

第9种方式：Method09 采用executor.submit()

第10种方式：Method10 采用CompletableFuture

### 作业二：

![avatar](./src/main/resources/static/多线程.jpg)