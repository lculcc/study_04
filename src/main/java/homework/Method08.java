package homework;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * Callable future
 */
class ThreadDemo implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        return fibo(36);
    }

    private int fibo(int a) {
        if ( a < 2)
            return 1;
        return fibo(a-1) + fibo(a-2);
    }
}
public class Method08 {


    public static void main(String[] args) throws ExecutionException, InterruptedException {

        long start=System.currentTimeMillis();

        ThreadDemo threadDemo = new ThreadDemo();
        FutureTask<Integer> futureTask = new FutureTask<>(threadDemo);
        // 在这里创建一个线程或线程池，
        // 异步执行 下面方法
        new Thread(futureTask).start();
        //这是得到的返回值
        int result = futureTask.get();
        // 确保  拿到result 并输出
        System.out.println("异步计算结果为：" + result);

        System.out.println("使用时间："+ (System.currentTimeMillis()-start) + " ms");
        // 然后退出main线程
    }



}
