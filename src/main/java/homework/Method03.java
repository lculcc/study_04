package homework;

/**
 * 采用同步块 + wait/notifyAll方式
 */
public class Method03 {

    //存储返回结果
    private volatile Integer value = null;

    private synchronized void sum(int num) {
        value = fibo(num);
        notifyAll();
    }

    private int fibo(int a) {
        if ( a < 2)
            return 1;
        return fibo(a-1) + fibo(a-2);
    }

    public synchronized int getValue() throws InterruptedException {
        //循环等待线程返回结果
        while (null == value){
            wait();
        }
        return value;
    }

    public static void main(String[] args) throws InterruptedException {

        long start=System.currentTimeMillis();

        Method03 method = new Method03();
        // 在这里创建一个线程或线程池，
        new Thread(() -> {
            method.sum(36);
        }).start();
        // 异步执行 下面方法

         //这是得到的返回值
        int result = method.getValue();
        // 确保  拿到result 并输出
        System.out.println("异步计算结果为：" + result);

        System.out.println("使用时间："+ (System.currentTimeMillis()-start) + " ms");
        // 然后退出main线程
    }



}
