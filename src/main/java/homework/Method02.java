package homework;

/**
 * 采用join
 */
public class Method02 {

    private volatile Integer value = null;

    //存储返回结果
    private void sum(int num) {
        value =  fibo(num);
    }

    private int fibo(int a) {
        if ( a < 2)
            return 1;
        return fibo(a-1) + fibo(a-2);
    }

    public static void main(String[] args) throws InterruptedException {

        long start=System.currentTimeMillis();

        Method02 method = new Method02();
        // 在这里创建一个线程或线程池，异步执行 下面方法
        Thread thread = new Thread(() -> {
            method.sum(36);
        });
        thread.start();
        thread.join();
         //这是得到的返回值
        int result = method.value;
        // 确保  拿到result 并输出
        System.out.println("异步计算结果为：" + result);

        System.out.println("使用时间："+ (System.currentTimeMillis()-start) + " ms");
        // 然后退出main线程
    }

}
